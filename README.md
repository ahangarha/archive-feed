# درباره

این پروژه، جهت بررسی خوراک feed وردپرس و بایگانی کردن مطالب جدید یا تغییریافته
 بر روی پایگاه archive.org است.

# About
This project is to analyze wordpress feed and archive new or modified pages on
archive.org website.
