#!/usr/bin/env python
# coding: utf-8

import time

import feedparser
from tinydb import TinyDB, Query
import requests

from config import *

db = TinyDB(DB_FILE)
feed = feedparser.parse(FEED_URL)

"""
These are keys of feed from wordpress standard feed:

feed.keys():
dict_keys(['feed',
           'entries',
           'bozo',
           'headers',
           'updated',
           'updated_parsed',
           'href',
           'status',
           'encoding',
           'version',
           'namespaces'])```

#### feed.entries[0].keys():
dict_keys(['title',
           'title_detail',
           'links',
           'link',
           'comments',
           'authors',
           'author',
           'author_detail',
           'published',
           'published_parsed',
           'tags',
           'id',
           'guidislink',
           'summary',
           'summary_detail',
           'wfw_commentrss',
           'slash_comments',
           'media_thumbnail',
           'href',
           'media_content'])
"""

query = Query()


def archive_link(link=""):
    if len(link) == 0:
        print("BAD LINK!!!")
        return False

    time.sleep(2)
    req = requests.get("https://web.archive.org/save/"+link)
    if req.status_code == 200 and "FILE ARCHIVED ON" in req.text:
        print("Page archived sucessfully!")
        return True
    else:
        if req.status_code != 200:
            print("FAILED: status code=", req.status_code)
        return False


for entry in feed.entries:
    """ As id field in wordpress feed is actually a link ending with post id,
    we need to process it"""
    post_id = entry.id.split("=")[-1]

    # can be checked if it is changed (removed from protection)
    title = entry.title

    # needed for saving on archive
    link = entry.link

    # check if there is a new entry
    if len(db.search(query.post_id == post_id)) == 0:
        print("Recording new entry in DB with id=", post_id)
        db.insert({
            'post_id': post_id,
            'title': title,
            'link': link,
        })
        archive_link(link)
        continue

    # check if there is change in title and if so, update DB and archive it
    recorded_entry = db.search(query.post_id == post_id)
    if len(recorded_entry) > 1:
        print("ALERT: there are more than one record for post_id=", post_id)

    if recorded_entry[0]['title'] != title:
        # title has changes so perhaps there is a change in the entry
        # update DB
        print("UPDATE: new title for post_id=", post_id)
        db.update({'title': title, 'link': link}, query.post_id == post_id)

        # archive it
        archive_link(link)
